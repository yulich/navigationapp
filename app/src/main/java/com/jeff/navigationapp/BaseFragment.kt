package com.jeff.navigationapp

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.log.JFLog

abstract class BaseFragment<B : ViewDataBinding, V : BaseViewModel> : Fragment() {

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    lateinit var binding: B
    lateinit var viewModel: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        JFLog.d(1,"onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        JFLog.d(1,"onCreateView")

        viewModel = ViewModelProviders.of(this)[getViewModelClass()]
        JFLog.i("$viewModel")

        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)

        // Inflate the layout for this fragment
        // val view = inflater.inflate(R.layout.fragment_other, container, false)

        onCreatedView(binding.root)

        return binding.root
    }

    override fun onPause() {
        super.onPause()

        JFLog.d(1, "onPause")
    }

    override fun onDestroyView() {
        super.onDestroyView()

        JFLog.d(1, "onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModelStore.clear()

        JFLog.d(1, "onDestroy")
    }

    abstract fun getLayoutRes() : Int

    abstract fun getViewModelClass(): Class<V>


    abstract fun onCreatedView(view: View)
}