package com.jeff.navigationapp

import androidx.lifecycle.ViewModel
import com.log.JFLog

open class BaseViewModel : ViewModel() {
    init {
        JFLog.w("init ViewModel")
    }
}